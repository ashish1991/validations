package com.example.demo.util;

import static java.lang.Boolean.TRUE;
import static org.springframework.beans.BeanUtils.copyProperties;

import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.dto.CustomerDto;
import com.example.demo.dto.GenericResponse;
import com.example.demo.dto.ProductDto;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Product;

public class ConvertUtil {

	private ConvertUtil() {
	}

	public static Customer convertDtoToEntity(CustomerDto dto) {
		final Customer entity = new Customer();
		copyProperties(dto, entity);
		entity.setProducts(dto.getProducts().stream().map(pr -> {
			Product product = new Product();
			product.setProductName(pr.getProductName());
			product.setProductPrice(pr.getProductPrice());
			return product;
		}).collect(Collectors.toList()));
		return entity;
	}

	public static CustomerDto convertEntityToDto(Customer entity) {
		CustomerDto dto = new CustomerDto();
		copyProperties(entity, dto);
		dto.setProducts(entity.getProducts().stream().map(pr -> {
			ProductDto productDto = new ProductDto();
			productDto.setProductName(pr.getProductName());
			productDto.setProductPrice(pr.getProductPrice());
			return productDto;
		}).collect(Collectors.toList()));
		return dto;
	}

	public static GenericResponse getGenericResponse(Object data) {
		GenericResponse response = new GenericResponse();
		response.setSuccess(TRUE);
		response.setData(data);
		return response;
	}

	public static GenericResponse getErrorGenericResponse(List<String> errors) {
		GenericResponse response = new GenericResponse();
		response.setErrors(errors);
		return response;
	}
}
