package com.example.demo.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductDto {
    private String productName;
    private BigDecimal productPrice;
}