package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

import com.example.demo.entity.Customer;

@Repository
@Transactional
public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
