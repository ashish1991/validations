package com.example.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.GenericResponse;
import com.example.demo.dto.CustomerDto;
import com.example.demo.service.CustomerService;

@RequestMapping("/customer")
@RestController
public class CustomerController {
	private final CustomerService userService;

	@Autowired
	public CustomerController(CustomerService userService) {
		this.userService = userService;
	}

	@PostMapping("/addUpdateCustomer")
	public ResponseEntity<GenericResponse> addUpdateCustomer(@RequestBody @Valid CustomerDto dto) {
		return ResponseEntity.ok(userService.addUpdateCustomer(dto));
	}

	@GetMapping("/testCustomException/{flag}")
	public ResponseEntity<GenericResponse> testCustomException(@PathVariable Boolean flag) {
		return ResponseEntity.ok(userService.testCustomException(flag));
	}
}
