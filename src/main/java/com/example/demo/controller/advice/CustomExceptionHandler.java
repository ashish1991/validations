package com.example.demo.controller.advice;

import static com.example.demo.util.ConvertUtil.getErrorGenericResponse;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.dto.GenericResponse;
import com.example.demo.exception.CustomerException;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		return ResponseEntity.badRequest().body(getErrorGenericResponse(
				ex.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(toList())));
	}

	@ExceptionHandler(CustomerException.class)
	public ResponseEntity<GenericResponse> handleUserException(RuntimeException ex) {
		return ResponseEntity.badRequest().body(getErrorGenericResponse(asList(ex.getMessage())));
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<GenericResponse> handleException(RuntimeException ex) {
		return ResponseEntity.badRequest().body(getErrorGenericResponse(asList(ex.getMessage())));
	}
}
