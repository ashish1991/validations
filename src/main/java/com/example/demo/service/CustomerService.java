package com.example.demo.service;

import com.example.demo.dto.GenericResponse;
import com.example.demo.dto.CustomerDto;

public interface CustomerService {
	GenericResponse addUpdateCustomer(CustomerDto dto);
	GenericResponse testCustomException(Boolean flag);
}
