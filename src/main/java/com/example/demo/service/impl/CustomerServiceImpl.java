package com.example.demo.service.impl;

import static com.example.demo.util.ConvertUtil.convertDtoToEntity;
import static com.example.demo.util.ConvertUtil.convertEntityToDto;
import static com.example.demo.util.ConvertUtil.getGenericResponse;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dto.GenericResponse;
import com.example.demo.dto.CustomerDto;
import com.example.demo.exception.CustomerException;
import com.example.demo.repository.CustomerRepository;
import com.example.demo.service.CustomerService;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Autowired
	public CustomerServiceImpl(CustomerRepository userRepository) {
		this.customerRepository = userRepository;
	}

	@Override
	public GenericResponse addUpdateCustomer(CustomerDto dto) {
		return getGenericResponse(convertEntityToDto(customerRepository.save(convertDtoToEntity(dto))));
	}

	@Override
	public GenericResponse testCustomException(Boolean flag) {
		if (!flag)
			throw new CustomerException("user error");
		return getGenericResponse("success");
	}
}
